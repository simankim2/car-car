import React from 'react';

class TechnicianForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            first_name: '',
            last_name: '',
            employee_id: '',
        };
        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleEmployeeIdChange = this.handleEmployeeIdChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleFirstNameChange(event) {
        const value = event.target.value;
        this.setState({ first_name: value })
    }

    handleLastNameChange(event) {
      const value = event.target.value;
      this.setState({ last_name: value })
    }

    handleEmployeeIdChange(event) {
        const value = event.target.value;
        this.setState({ employee_id: value })
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            const cleared = {
                first_name: '',
                last_name: '',
                employee_id: '',
            };
            this.setState(cleared)
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1 style={{ fontSize: "42px", paddingTop: "30px", paddingBottom: "10px" }}>Add a New Technician</h1>
                        <form onSubmit={this.handleSubmit} id="create-technician-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFirstNameChange} value={this.state.first_name} placeholder="First Name" required type="text" first_name="first_name" id="first_name" className="form-control" />
                                <label htmlFor="first_name">First Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleLastNameChange} value={this.state.last_name} placeholder="Last Name" required type="text" last_name="last_name" id="last_name" className="form-control" />
                                <label htmlFor="last_name">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleEmployeeIdChange} value={this.state.employee_id} placeholder="Employee Id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                                <label htmlFor="employeeNumber">Employee Id</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default TechnicianForm
