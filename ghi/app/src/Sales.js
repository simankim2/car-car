import React, { useEffect, useState } from "react";

function Sales() {
    const [salesData, setSalesData] = useState([]);

    useEffect(() => {
        async function fetchData() {
            try {
                const url = "http://localhost:8090/api/sales/";
                const response = await fetch(url);
                if (!response.ok) {
                    alert("Bad response!");
                } else {
                    const data = await response.json();
                    setSalesData(data.sales);
                }
            } catch (e) {
                alert("Error was raised!")
            }
        }

        fetchData();
    }, []);

    return (
        <div>
            <h1 style={{ fontSize: "42px", paddingTop: "30px", paddingBottom: "10px" }}>Sales</h1>
            <table style={{ width: "100%", borderCollapse: "collapse" }}>
                <thead>
                    <tr>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Salesperson Employee ID</th>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Salesperson Name</th>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Customer</th>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>VIN</th>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salesData.map((sales, index) => (
                        <tr key={index} style={{ backgroundColor: index % 2 === 1 ? "#f2f2f2" : "white" }}>
                            <td>{sales.salesperson.employee_id}</td>
                            <td>{sales.salesperson.first_name} {sales.salesperson.last_name}</td>
                            <td>{sales.customer.first_name} {sales.customer.last_name}</td>
                            <td>{sales.automobile.vin}</td>
                            <td>${sales.price}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default Sales;