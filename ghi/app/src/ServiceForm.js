import React from 'react';

class ServiceForm extends React.Component{

constructor(props) {
    super(props)
    this.state = {
        vin:'',
        customer:'',
        date_time:'',
        technicians:[],
        reason:'',
        vip:false,
    };
        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleCustomerChange = this.handleCustomerChange.bind(this);
        this.handleDate_TimeChange = this.handleDate_TimeChange.bind(this);
        this.handleTechnicianChange = this.handleTechnicianChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleVinChange(event) {
        const value = event.target.value;
        this.setState({vin:value})
    }

    handleCustomerChange(event) {
    const value = event.target.value;
    this.setState({customer:value})
    }

    handleDate_TimeChange(event) {
    const value = event.target.value;
    this.setState({date_time:value})
    }

    handleTechnicianChange(event) {
    const value = event.target.value;
    this.setState({technician:value})
    }

    handleReasonChange(event) {
    const value = event.target.value;
    this.setState({reason:value})
    }

    async componentDidMount() {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({technicians: data.technicians});
        }
    }
    handleReasonChange(event) {
        const value = event.target.value;
        this.setState({ reason: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        delete data.technicians
        const serviceUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        header: {
            'Content-Type': 'application/json'
        }}
        const response = await fetch(serviceUrl, fetchConfig);
        if(response.ok) {
            const newService = await response.json();
        this.setState ({
            vin:'',
            customer:'',
            date_time:'',
            technician:'',
            reason:'',
            vip:false,
        });
    }
    }
    render() {
    let messageClasses = "alert alert-success mb-0 d-none"
    let formClasses = ""
    if (this.state.hasMadeAppt){
        messageClasses = "alert alert-success mb-0"
        formClasses = "d-none"
    }
    function reload(){
        window.location.reload()
    }
    return(
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create Service Appointment</h1>
                <form className={formClasses} onSubmit={this.handleSubmit} id="create-service-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleVinChange} value = {this.state.vin} placeholder="vin" required type="text" id="vin" name="vin" className="form-control" />
                    <label htmlFor="vin">Vin number #</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleCustomerChange} value = {this.state.owner} placeholder="customer" required type="text" id="customer" name="customer" className="form-control"/>
                    <label htmlFor="customer">Customer's Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleDate_TimeChange} value = {this.state.date_time} placeholder="date_time" required type="datetime-local" name="date_time" id="date_time" className="form-control" />
                    <label htmlFor="date_time">Date of Service</label>
                </div>
                <div className="form-floating mb-3">
                <select onChange={this.handleTechnicianChange} required name="technicians" id="technicians" className="form-select">
                                <option value="">Choose a technician</option>
                                {this.state.technicians.map(tech => {
                                    return (<option key={tech.employee_id} value={tech.employee_id}>{tech.last_name}</option>
                                    )
                                })}
                            </select>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleReasonChange} value = {this.state.reason} placeholder="Reason for the service" required type="text" name="reason" id="reason" className="form-control" />
                    <label htmlFor="reason">Service</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
                <div className={messageClasses}>
                <h3>Appointment registered!</h3>
                <button onClick={reload} className="btn btn-outline-success">Go Back</button>
                </div>
            </div>
        </div>
      </div>
    )
    }
}

export default ServiceForm;
