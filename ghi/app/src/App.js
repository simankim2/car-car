import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ServiceHistory from './ServiceHistory';
import ServiceForm from './ServiceForm';
import ServiceAppointmentList from './ServiceAppointmentList';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import AddaSalesperson from "./AddaSalesperson";
import SalesPeople from "./SalesPeople";
import AddaCustomer from "./AddaCustomer";
import Customers from "./Customers";
import AddaSale from "./AddaSale";
import Sales from "./Sales";
import SalesHistory from "./SalesHistory";
import CreateManufacturer from "./CreateManufacturer";
import ListManufacturers from "./ListManufacturers";
import CreateVehicleModel from "./CreateVehicleModel";
import VehicleModelList from "./VehicleModelList";
import CreateAutomobile from "./CreateAutomobile";
import AutomobileList from "./AutomobileList";


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/service/history" element={<ServiceHistory />} />
          <Route path="/appointments" element={<ServiceForm />} />
          <Route path="/appointments/list" element={<ServiceAppointmentList />} />
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians" element={<TechnicianList />} />
          <Route path="/technicians/new" element={<TechnicianForm />} />
          <Route path="/salespeople" element={<SalesPeople />} />
          <Route path="/salespeople/create" element={<AddaSalesperson />} />
          <Route path="/customers" element={<Customers />} />
          <Route path="/customers/create" element={<AddaCustomer />} />
          <Route path="/sales" element={<Sales />} />
          <Route path="/sales/create" element={<AddaSale />} />
          <Route path="/sales/history" element={<SalesHistory />} />
          <Route path="/manufacturers/create" element={<CreateManufacturer />} />
          <Route path="/manufacturers" element={<ListManufacturers />} />
          <Route path="/models/create" element={<CreateVehicleModel />} />
          <Route path="models" element={<VehicleModelList />} />
          <Route path="/automobiles/create" element={<CreateAutomobile />} />
          <Route path="/automobiles" element={<AutomobileList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
