import React from 'react';

class ServiceAppointmentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
        }
    }
    async componentDidMount() {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ appointments: data.appointments.filter(appt => !appt.active) })
        }
    }
    async cancelService(id, customer) {
        const isConfirmed = window.confirm(`Cancel this appointment for ${customer}?`)
        if (isConfirmed) {
            const url = `http://localhost:8080/api/appointments/${id}/`;
            const fetchConfig = { method: "PUT" };
            const response = await fetch(url, fetchConfig);
            const filteredAppointments = this.state.appointments.filter(appointment => appointment.id !== id)
            if (response.ok) {
                this.setState({ appointments: filteredAppointments })
            }
        }
    }
    async finishedService(id, customer) {
            const URL = `http://localhost:8080/api/appointments/${id}/`;
            const data = { active: true };
            const fetchConfig = { method: "PUT", body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'} };
            const finishedResponse = await fetch(URL, fetchConfig);
            const filteredAppointments = this.state.appointments.filter(appointment => appointment.id !== id)
            if (finishedResponse.ok) {
                this.setState({ appointments: filteredAppointments })
        };
    }
    render() {
    return(
        <div>
            <h1 style={{ fontSize: "42px", paddingTop: "30px", paddingBottom: "10px" }}>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Vin</th>
                    <th>Owner</th>
                    <th>Vip</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Complete</th>
                </tr>
                </thead>
                <tbody>
                {this.state.appointments.map((appointment,i )=> {
                    return (
                    <tr key={appointment.id}>
                        <td>{appointment.vin}</td>
                        <td>{appointment.customer}</td>
                        <td>{appointment.vip ? 'VIP': false}</td>
                        <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                        <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                        <td>{appointment.technician.first_name}</td>
                        <td>{appointment.reason}</td>
                        <td><button className="btn btn-success" onClick={() => this.finishedService(appointment.id)} type="button">Finish</button></td>
                        <td><button className="btn btn-danger" onClick={() => this.cancelService(appointment.id)} type="button">Cancel</button></td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
    )};
}

export default ServiceAppointmentList;
