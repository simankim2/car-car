import React, { useState } from "react";

function AddaCustomer() {
  const [formData, setFormData] = useState({
    first_name: "",
    last_name: "",
    address: "",
    phone_number: "",
  });

  const [isLoading, setIsLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true);

    const url = "http://localhost:8090/api/customers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setSuccessMessage("Customer added successfully!");
        setFormData({
          first_name: "",
          last_name: "",
          address: "",
          phone_number: "",
        });
      } else {
        setErrorMessage("Failed to add the customer.");
      }
    } catch (e) {
      setErrorMessage("Error occurred while adding the customer.");
    } finally {
      setIsLoading(false);
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Customer</h1>
          {errorMessage && <div className="alert alert-danger">{errorMessage}</div>}
          {successMessage && <div className="alert alert-success">{successMessage}</div>}
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-1">
              <input onChange={handleFormChange} placeholder="First name" required type="text" name="first_name" id="first_name" className="form-control" value={formData.first_name} />
              <label htmlFor="first_name">First name...</label>
            </div>
            <div className="form-floating mb-1">
              <input onChange={handleFormChange} placeholder="Last name" required type="text" name="last_name" id="last_name" className="form-control" value={formData.last_name} />
              <label htmlFor="last_name">Last name...</label>
            </div>
            <div className="form-floating mb-4">
              <input onChange={handleFormChange} placeholder="Address" required type="text" name="address" id="address" className="form-control" value={formData.address} />
              <label htmlFor="address">Address...</label>
            </div>
            <div className="form-floating mb-4">
              <input onChange={handleFormChange} placeholder="Phone number" required type="text" name="phone_number" id="phone_number" className="form-control" value={formData.phone_number} />
              <label htmlFor="phone_number">Phone number...</label>
            </div>
            <button className="btn btn-primary" disabled={isLoading}>
              {isLoading ? "Adding..." : "Create"}
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AddaCustomer;
