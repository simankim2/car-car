import React from "react";

class ServiceHistory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vin:[],
            appointments: [],
        }
        this.handleChangeVin = this.handleChangeVin.bind(this);
    }


    async componentDidMount() {

        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const appointmentUrl = 'http://localhost:8080/api/appointments/';


        const appointmentResponse = await fetch(appointmentUrl);
        const automobileResponse = await fetch(automobileUrl);

        console.log(appointmentResponse)

        if (appointmentResponse.ok && automobileResponse.ok) {
            const appointmentData = await appointmentResponse.json();
            const automobileData = await automobileResponse.json();

            console.log(appointmentData);
            this.setState({
                appointments: appointmentData.appointments,
                automobile: automobileData.autos
            })
        }
    }

    handleChangeVin(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }

    render() {
        return (
            <div className="container">
            <h2 style={{ fontSize: "42px", paddingTop: "30px", paddingBottom: "10px" }}>Service History</h2>
            <div className="row">
                <div className="mb-3">
                    <select onChange={this.handleChangeVin} value={this.state.vin} required name="name" id="name" className="form-select">
                        <option value="">Choose a VIN number</option>
                        {this.state.automobile?.map(automobile => {
                            return (
                            <option key={automobile.href} value={automobile.vin}>{automobile.vin}</option>
                            )
                            })}
                    </select>
                </div>
            <table className="table table-striped">
            <thead>
              <tr>
                <th>Vin</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Date</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {this.state.appointments && this.state.appointments.filter(appointments => appointments.vin.vin === this.state.vin).map(appointments => {
                  return (
                    <tr key={appointments.href}>
                      <td>{ appointments.vin }</td>
                      <td>{ appointments.first_name }</td>
                      <td>{ appointments.last_name }</td>
                      <td>{ appointments.date_time }</td>
                      <td>{ appointments.technician }</td>
                      <td>{ appointments.reason }</td>
                      <td>{ appointments.status }</td>

                    </tr>
                  );
              })}
            </tbody>
          </table>
          </div>
          </div>
        )
    }
}

export default ServiceHistory;
