import React from "react";
import { Link } from "react-router-dom";

class ListManufacturers extends React.Component {
  constructor(props) {
    super(props);
    this.state = { manufacturers: [] };
  }
  async componentDidMount() {
    const response = await fetch("http://localhost:8100/api/manufacturers/");
    if (response.ok) {
      const data = await response.json();
      this.setState({ manufacturers: data.manufacturers });
    }
  }

  render() {
    return (
      <div className="container mt-3">
        <h1 style={{ fontSize: "42px", paddingTop: "30px", paddingBottom: "10px" }}>Manufacturers</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Name</th>
            </tr>
          </thead>
          <tbody>
            {this.state.manufacturers.map((manufacturer) => {
              return (
                <tr key={manufacturer.id}>
                  <td>{manufacturer.name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
          <Link to="/manufacturer/create" className="btn btn-primary">
            Add a manufacturer
          </Link>
        </div>
      </div>
    );
  }
}
export default ListManufacturers;
