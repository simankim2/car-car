import React, { useEffect, useState } from "react";

function Customers() {
    const [customersData, setCustomersData] = useState([]);

    useEffect(() => {
        async function fetchData() {
            try {
                const url = "http://localhost:8090/api/customers/";
                const response = await fetch(url);
                if (!response.ok) {
                    alert("Bad response!");
                } else {
                    const data = await response.json();
                    setCustomersData(data.customers);
                }
            } catch (e) {
                alert("Error was raised!")
            }
        }

        fetchData();
    }, []);

    return (
        <div>
            <h1 style={{ fontSize: "42px", paddingTop: "30px", paddingBottom: "10px" }}>Customers</h1>
            <table style={{ width: "100%", borderCollapse: "collapse" }}>
                <thead>
                    <tr>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>First Name</th>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Last Name</th>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Phone Number</th>
                        <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customersData.map((customer, index) => (
                        <tr key={index} style={{ backgroundColor: index % 2 === 1 ? "#f2f2f2" : "white" }}>
                            <td>{customer.first_name}</td>
                            <td>{customer.last_name}</td>
                            <td>{customer.phone_number}</td>
                            <td>{customer.address}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default Customers;