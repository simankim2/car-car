import React, { useState } from "react";

function CreateManufacturer() {
  const [formData, setFormData] = useState({
    name: "",
  });

  const [isLoading, setIsLoading] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true);

    const url = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setSuccessMessage("Manufacturer created successfully!");
        setFormData({
          name: "",
        });
      } else {
        setErrorMessage("Failed to create the manufacturer.");
      }
    } catch (e) {
      setErrorMessage("Error occurred while creating the manufacturer.");
    } finally {
      setIsLoading(false);
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a manufacturer</h1>
          {errorMessage && <div className="alert alert-danger">{errorMessage}</div>}
          {successMessage && <div className="alert alert-success">{successMessage}</div>}
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-1">
              <input onChange={handleFormChange} placeholder="name" required type="text" name="name" id="name" className="form-control" value={formData.name} />
              <label htmlFor="name">Manufacturer name...</label>
            </div>
            <button className="btn btn-primary" disabled={isLoading}>
              {isLoading ? "Adding..." : "Create Manufacturer"}
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateManufacturer;
