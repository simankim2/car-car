import React from 'react';

class VehicleModelsList extends React.Component {
    constructor(props){
        super(props);
        this.state={
            models: [],
        };
    }

    async componentDidMount() {
        const URL = 'http://localhost:8100/api/models/'
        const response = await fetch(URL);
        if (response.ok) {
            const data = await response.json();
            this.setState({ models: data.models });
        }
    }
    render() {
        return (
        <div>
        <h1 style={{ fontSize: "42px", paddingTop: "30px", paddingBottom: "10px" }}>Vehicle Models</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Vehicle Name</th>
                    <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Manufacturer</th>
                    <th style={{ borderBottom: "1px solid black", textAlign: "left" }}>Picture</th>
                </tr>
            </thead>
        <tbody>
           {this.state.models.map((model) => {
               return (
                   <tr key={ model.href}>
                        <td>{ model.name }</td>
                        <td>{ model.manufacturer.name}</td>
                        <td><img src={ model.picture_url} /> </td>
                    </tr>
            );
        })}
        </tbody>
      </table>
    </div>
    );
  }
}


export default VehicleModelsList
