from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    employee_id = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return f"{self.last_name}, {self.first_name} : {self.employee_id}"

    class Meta:
        ordering = ("last_name",)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    date_time = models.DateTimeField(auto_now_add=True)
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=10)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=50)
    vip = models.BooleanField(default=False)

    technician = models.ForeignKey(
        Technician,
        related_name="technicians",
        on_delete=models.CASCADE,
    )

    def cancel(self):
        self.status = "canceled"
        self.save()

    def finish(self):
        self.status = "finished"
        self.save()

    def get_api_url(self):
        return reverse("api_show_appointments", kwargs={"id": self.id})

    def __str__(self):
        return self.status

    class Meta:
        ordering = ("date_time", "customer")
